#!/bin/bash
source ./venv/bin/activate
set -a
source .env
set +a
python cfcc_api/manage.py runserver
deactivate
