# CF Coding Challenge

## Getting started

- Must have python 3.8 with virtualenv already installed
- Run `./setup.sh` to create a python virtual environment and setup the database
  - Don't forget to add your Google Geocode API key to the newly created .env file
- Run `./runserver.sh` run the server locally
- Run `./reset.sh` to remove out the virtualenv and database

## Endpoints and Payloads
### Single Location search
```
HTTP GET http://127.0.0.1:8000/address-search
```

##### JSON payloads
Text search
```
{
    "format": "text",
    "location": "Phoenix, AZ"
}
```

Coordinate search
```
{
    "format": "cordinates",
    "location": "32.2226066,-110.9747108"
}
```

##### Example return payload
```
{
    "name": "Phoenix, AZ, USA",
    "latitude": "33.448377100000",
    "longitude": "-112.074037300000",
    "search_text": "Phoenix, AZ"
}
```

### Two Location Trip search
```
HTTP GET http://127.0.0.1:8000/address-distance
```

##### JSON payloads

Text search
```
{
    "format": "text",
    "location_1": "Phoenix, AZ",
    "location_2": "Tucson, AZ",
    "is_metric": true
}
```

Coordinate search
```
{
    "format": "cordinates",
    "location_1": "33.448377, -112.07403",
    "location_2": "32.2226, -110.974",
    "is_metric": false
}
```

##### Example return payload
```
{
    "address_1": {
        "name": "Madrid, Spain",
        "latitude": "40.416775400000",
        "longitude": "-3.703790200000",
        "search_text": "Madrid, Spain"
    },
    "address_2": {
        "name": "Christchurch, New Zealand",
        "latitude": "-43.532021400000",
        "longitude": "172.630558900000",
        "search_text": "Christchurch, New Zealand"
    },
    "distance": 12150.894800010756,
    "units": "Mi"
}
```
"is_metric" is an optional key
