#!/bin/bash
deactivate
rm -rf ./venv
rm cfcc_api/db.sqlite3
echo ""
echo ""
echo "Local configuration cleared"
echo "You can now run the setup.sh again"
echo ""
echo ""

