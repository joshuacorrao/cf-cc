#!/bin/bash
cp sample.env .env
python3 -m virtualenv -p python3 venv
source ./venv/bin/activate
pip install -r requirements.txt
python cfcc_api/manage.py migrate
deactivate
echo ""
echo ""
echo "Dont forget to modify GOOGLE_API_KEY in your new .env file!!"
echo ""
echo ""
