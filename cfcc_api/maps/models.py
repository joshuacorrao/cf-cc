from django.db import models


class Address(models.Model):
    name = models.CharField(max_length=255, null=False, blank=False)
    latitude = models.DecimalField(max_digits=15, decimal_places=12, null=False)
    longitude = models.DecimalField(max_digits=15, decimal_places=12, null=False)


class SearchTerm(models.Model):
    text = models.CharField(max_length=255, null=False, blank=False)
    address = models.ForeignKey(Address, on_delete=models.CASCADE, related_name="search_term")
