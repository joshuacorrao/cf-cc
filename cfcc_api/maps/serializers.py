import logging

from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from rest_framework.fields import CharField, ChoiceField, BooleanField

from maps.models import Address


LOG = logging.getLogger(__name__)


class AddressSerializer(serializers.ModelSerializer):
    class Meta:
        model = Address
        fields = ["name", "latitude", "longitude"]


class AddressSearchSerializer(serializers.Serializer):
    format = ChoiceField(choices=["text", "coordinates"])  # TODO maybe add a choices meta class
    location = CharField(max_length=255)

    def validate(self, data):
        validated_data = super().validate(data)
        if validated_data["format"] == "coordinates":
            validate_coordinates(validated_data["location"])

        return validated_data


class AddressDistanceSerializer(serializers.Serializer):
    format = ChoiceField(choices=["text", "coordinates"])  # TODO maybe add a choices meta class
    location_1 = CharField(max_length=255)
    location_2 = CharField(max_length=255)
    metric = BooleanField(default=True)

    def validate(self, data):
        validated_data = super().validate(data)
        if validated_data["format"] == "coordinates":
            validate_coordinates(validated_data["location_1"])
            validate_coordinates(validated_data["location_2"])

        return validated_data


def validate_coordinates(data: str):
    """Raise a ValidationError if coordinates are not formatted correctly"""

    LOG.warning("validating coodinates")
    try:
        coords_raw = data.replace(" ", "").split(",")
        _ = list(map(float, coords_raw))
    except Exception as e:
        LOG.warning(e)
        raise ValidationError("Invalid coordinates format")
