import logging

from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from maps.serializers import AddressDistanceSerializer, AddressSearchSerializer
from maps.services import AddressSearcher, TripEvaluator


LOG = logging.getLogger(__name__)


class AddressSearchViewSet(APIView):

    def get(self, request):

        serializer = AddressSearchSerializer(data=request.data)
        if serializer.is_valid():
            location_text = request.data["location"]
            query_format = request.data["format"]

            address_searcher = AddressSearcher(location_text, query_format)
            return address_searcher.serialize()

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class AddressDistanceViewSet(APIView):

    def get(self, request):

        serializer = AddressDistanceSerializer(data=request.data)
        if serializer.is_valid():
            location_1_text = request.data["location_1"]
            location_2_text = request.data["location_2"]
            query_format = request.data["format"]
            is_metric = request.data.get("is_metric", True)

            trip_evaluator = TripEvaluator(location_1_text, location_2_text, query_format)
            return trip_evaluator.serialize(is_metric=is_metric)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
