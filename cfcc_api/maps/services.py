import logging
from math import sin, cos, sqrt, asin, radians

import requests
from django.conf import settings
from rest_framework import status
from rest_framework.response import Response

from maps.models import Address, SearchTerm
from maps.serializers import AddressSerializer


LOG = logging.getLogger(__name__)


class AddressSearcher:
    """Looks up an address by search term locally, if not found, query Google and save locally"""

    search_text: str
    address: Address
    _address: Address = None

    def __init__(self, search_text: str, query_format: str = "text"):
        self.search_text = search_text
        self.query_format = query_format

    @property
    def address(self) -> Address:
        """Look up address from search text, acquire and save from Google first if not found"""

        if not self._address:
            try:
                search_term = SearchTerm.objects.get(text=self.search_text)
                self._address = search_term.address
                LOG.warning("ADDRESS FOUND!")

            except SearchTerm.DoesNotExist:  # the search term has not been used before
                LOG.warning("No cached address. Querying Google")
                self._address = self._get_google_address(self.search_text, self.query_format)

                if self._address:
                    search_term = SearchTerm(
                        text=self.search_text,
                        address_id=self._address.id,
                    )
                    search_term.save()

        return self._address

    @staticmethod
    def _get_google_address(text: str, query_format="text") -> Address:
        """Query Google Maps API and create an address locally from the results"""

        params = {
            "key": settings.GMAPS_API_KEY,
        }

        if query_format == "text":
            LOG.warning("running text search")
            params["address"] = text.replace(" ", "+")  # format the search text for Google API
        if query_format == "coordinates":
            LOG.warning("Running coords search")
            params["latlng"] = text.replace(" ", "")

        base_url = settings.GMAPS_BASE_URL
        r = requests.get(base_url, params=params)

        if not (result := r.json()["results"][:1]):  # only grabbing first result
            return  # return None if Google found nothing

        address_data = result[0]
        formatted_address = address_data["formatted_address"]
        latitude = address_data["geometry"]["location"]["lat"]
        longitude = address_data["geometry"]["location"]["lng"]

        address = Address(
            name=formatted_address,
            latitude=latitude,
            longitude=longitude
        )
        address.save()

        return address

    def serialize(self):
        """Generate a response object based on if an address was found or not"""

        if self.address:
            address_data = AddressSerializer(self.address).data
            address_data["search_text"] = self.search_text
            return Response(address_data, status=status.HTTP_200_OK)
        else:
            return Response("Unable to locate address with supplied payload", status=status.HTTP_404_NOT_FOUND)


class TripEvaluator:
    """Find two addresses and calculate the distance between them, uses AddressSearcher to find addresses"""

    location_1_text: str
    location_2_text: str
    address_1: Address
    address_2: Address
    distance: float
    _distance: float = None

    def __init__(self, location_1_text: str, location_2_text: str, query_format: str = "text"):
        self.location_1_text = location_1_text
        self.location_2_text = location_2_text
        self.address_1 = AddressSearcher(self.location_1_text, query_format).address
        self.address_2 = AddressSearcher(self.location_2_text, query_format).address

    @property
    def distance(self) -> float:
        """Just do some math here. Haversine formula, too big for my brain"""

        if (not self._distance) and (self.address_1 and self.address_2):
            r = 6371.009  # radius of earth in KM

            # convert decimal degrees to radians
            lat1 = radians(float(self.address_1.latitude))
            lon1 = radians(float(self.address_1.longitude))
            lat2 = radians(float(self.address_2.latitude))
            lon2 = radians(float(self.address_2.longitude))

            # haversine formula
            d_lon = lon2 - lon1
            d_lat = lat2 - lat1
            a = sin(d_lat / 2) ** 2 + cos(lat1) * cos(lat2) * sin(d_lon / 2) ** 2
            c = 2 * asin(sqrt(a))

            self._distance = c * r

        return self._distance

    def serialize(self, is_metric=True):
        """Generate a response object based on the trip's address data

        Return both addresses and distance between them if both addresses found
        If either of the addresses are not found return a 404 with a payload informing which location text(s) could not
        find an address
        """

        if self.address_1 and self.address_2:  # both addresses found
            address_1_data = AddressSerializer(self.address_1).data
            address_2_data = AddressSerializer(self.address_2).data

            # add search text strings to the payloads
            address_1_data["search_text"] = self.location_1_text
            address_2_data["search_text"] = self.location_2_text

            if is_metric is False:
                distance = self.distance * 0.621371
                units = "Mi"
            else:
                distance = self.distance
                units = "KM"

            response_data = {
                "address_1": address_1_data,
                "address_2": address_2_data,
                "distance": distance,
                "units": units
            }

            return Response(response_data, status=status.HTTP_200_OK)
        else:
            response_data = {}
            if not self.address_1:
                response_data["location_1"] = f"Unable to locate: {self.location_1_text}"
            if not self.address_2:
                response_data["location_2"] = f"Unable to locate: {self.location_2_text}"

            return Response(response_data, status=status.HTTP_404_NOT_FOUND)
