# Generated by Django 3.2.7 on 2021-09-09 02:01

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name="Address",
            fields=[
                ("id", models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name="ID")),
                ("name", models.CharField(max_length=255)),
                ("latitude", models.DecimalField(decimal_places=15, max_digits=12)),
                ("longitude", models.DecimalField(decimal_places=15, max_digits=12)),
            ],
        ),
        migrations.CreateModel(
            name="SearchTerm",
            fields=[
                ("id", models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name="ID")),
                ("text", models.CharField(max_length=255)),
                ("address", models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name="search_term", to="maps.address")),
            ],
        ),
    ]
